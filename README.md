# docker-laravel-vue
Dockerを使用したLaravelとVue.jsの開発環境
- Nginx
- PHP
- Node.js
- PostgreSQL

## Requirement
- [docker-reverse-proxy](https://gitlab.com/ys_sakai/docker-reverse-proxy)

## Usage
##### 1. srcディレクトリを作成する

```sh
mkdir src
```

##### 2. Laravelをインストールする

```sh
docker-compose run --rm php bash
cd ..
composer create-project --prefer-dist laravel/laravel src "5.5.*"
```

##### 3. .envファイルを編集する

```
DB_CONNECTION=pgsql
DB_HOST=postgres
DB_PORT=5432
DB_DATABASE=root
DB_USERNAME=root
DB_PASSWORD=root
```

##### 4. 各種コマンドを使用する

```sh
# 各デーモン系コンテナの起動
docker-compose up -d

# Composerコマンドの使用
docker-compose run --rm php composer [command]

# npmコマンドの使用
docker-compose run --rm node npm [command]

# Laravel Artisanコマンドの使用
docker-compose run --rm php php artisan [command]
# コンテナ起動時には
docker-compose exec php php artisan [command]
```

## 参考
- [Laravelで最適なDocker環境を作る](https://qiita.com/kotamat/items/9f2fe9cc66cf3536abe5)
- [Laravelの開発に最適なDockerCompose作ってみた](http://m2wasabi.hatenablog.com/entry/2017/12/02/laravel-docker-adv)
- [Dockerでいい感じにPHP(Laravel)のローカル開発環境を作る](https://qiita.com/IganinTea/items/aec8f2b15b203946a2c4)
